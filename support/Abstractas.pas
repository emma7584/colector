unit Abstractas;

interface

type
  AListableClass = class abstract(TObject)
    public
      descripcion: string;
      fontColor: System.Cardinal;
      fontSize: integer;
      constructor Create();
  end;

implementation

constructor AListableClass.Create();
begin
  self.fontColor := $FF000000;
  self.fontSize := 14;
end;

end.

