unit Globales;

interface

uses
  FMX.ListBox, System.Generics.Collections,
  FMX.Types, FireDAC.Comp.Client,
  SysUtils, System.DateUtils, REST.Client, System.TimeSpan,
  System.IOUtils, System.Classes, System.Diagnostics,
  FMX.Dialogs, System.Bluetooth, Abstractas;

const
  FONT_COLOR_VERDE = $FF1C9D43;
  FONT_COLOR_ROJO = $FFD8473E;
  COLORVENTAS_PRINCIPAL = $FF22B8D1;
  COLORVENTAS_CLARO = $FF47D4E2;
  COLORVENTAS_FONDO = $FFD3D3D3;
  COLORVENTAS_STATUSBAR = $FF1FA7BF;
  URL_ORIGEN_DESTINO = 'https://www.google.com/maps/dir/%2.6f,%2.6f/%2.6f,%2.6f';
  ERRORLOGFILE = 'errores.txt';
  DLG_CERRAR = '�Desea cerrar la aplicaci�n?';

type
  TCodigoColectado = class
    id: string;
    codigo: string;
    fecha: string;
    nombre: string;
    precio: Real;
  end;

  TCabeceraColectada = class
    id: string;
    codigo: string;
    fecha: string;
    nombre: string;
    precio: Double;
  end;


function gObtenerGUID32(): string;
function gGetFileSize(const AFileName: string): Int64;

function gObtenerCarpetaDatos: string;
function gObtenerRutaArchivoConfiguracion(usuario: string): string;
function gObtenerRutaArchivoBD(usuario: string): string;
function gObtenerRutaArchivoBDZip: string;
function gObtenerRutaArchivo(nombre: string): string;
function gDateToStr(d: TDateTime; defaultTime: string = ''): string;
function gDateToStrQuoted(d: TDateTime; defaultTime: string = ''): string;
function gGetJsonStatusStr(response: TRESTResponse): string;
function gGetPublicPath: string;
function gToBool(x: integer): Boolean;

function gCargarCombo(cmb: TComboBox; elementos: TObjectList<AListableClass>): TComboBox;
function gEstaConectado: boolean;
procedure gGuardarLOGError(texto: String);
procedure gSplit(Delimiter: Char; Str: string; ListOfStrings: TStrings);

implementation

uses
{$IFDEF Android}
  NetworkState,
{$ENDIF}
  System.Math, System.JSON, REST.Types;

procedure gSplit(Delimiter: Char; Str: string; ListOfStrings: TStrings) ;
begin
   ListOfStrings.Clear;
   ListOfStrings.Delimiter       := Delimiter;
   ListOfStrings.StrictDelimiter := True; // Requires D2006 or newer.
   ListOfStrings.DelimitedText   := Str;
end;

procedure gGuardarLOGError(texto: String);
begin
  try
    {$IFDEF Android}
      TFile.AppendAllText(TPath.Combine(TPath.GetPublicPath, ERRORLOGFILE), #13#10 +
      FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': ' + texto);
    {$ELSE}
      TFile.AppendAllText(gObtenerRutaArchivo(ERRORLOGFILE), #13#10 +
      FormatDateTime('dd/mm/yy hh:MM:ss', now) + ': ' + texto);
    {$ENDIF}
  finally

  end;
end;

function gGetFileSize(const AFileName: string): Int64;
var
  LStrm: TFileStream;
begin
  LStrm := TFileStream.Create(AFileName, fmOpenRead and fmShareDenyNone);
  // alternatively: LStrm := TFile.OpenRead(AFileName);
  try
    Result := LStrm.Size;
  finally
    LStrm.Free;
  end;
end;

// http://delphiworlds.com/2013/11/checking-for-an-internet-connection-on-mobile-devices-with-delphi-xe5/
function gEstaConectado: boolean;
{$IFDEF Android}
var
  NS: TNetworkState;
begin
  Result := False;
  NS := TNetworkState.Create;
  try
    if not NS.IsConnected then
      Result := False
    else if NS.IsWifiConnected then
      Result := True
    else if NS.IsMobileConnected then
      Result := True
  finally
    NS.Free;
  end;
{$ELSE}

begin
  Result := True; // Windows
{$ENDIF}
end;

function gObtenerGUID32(): string;
var
  guid: TGUID;
  sGUID, sUUID: String;
begin
  CreateGUID(guid);
  sGUID := LowerCase(GUIDToString(guid));
  if Length(sGUID) = 38 then
  begin
    sUUID := copy(sGUID, 2, 36);
    sUUID := stringreplace(sUUID, '-', '', [rfReplaceAll]);
    Result := sUUID;
  end
  else
    raise exception.Create('Longitud invalida creando GUID');
end;

function gDateToStr(d: TDateTime; defaultTime: string): string;
begin
  if defaultTime = '' then
    Result := FormatDateTime('yyyy-mm-dd', d) + 'T' +
      FormatDateTime('hh:nn:ss.zzz', d)
  else
    Result := FormatDateTime('yyyy-mm-dd', date) + 'T' + defaultTime;
end;

function gDateToStrQuoted(d: TDateTime;
  defaultTime: string): string;
begin
  Result := QuotedStr(gDateToStr(d, defaultTime));
end;

function gGetJsonStatusStr(response: TRESTResponse): string;
var
  requestError: string;
begin
  if response.StatusCode = 200 then
  begin
    Result := '200 - OK';
  end
  else
  begin
    case response.StatusCode of
      403:
        requestError := 'Forbidden access to Private section';
      460:
        requestError := 'User already registered';
      461:
        requestError := 'Not Found by required parameters';
      462:
        requestError := 'Invalid user session id';
      463:
        requestError := 'Name cannot be empty';
      464:
        requestError := 'Ip and Key cannot be null at the same time';
      465:
        requestError := 'Current available calls for ApiKey is 0';
      466:
        requestError := 'Ip request day limit';
      500:
        requestError := 'Internal server exception';
    else
      requestError := 'WS Error - ' + response.StatusCode.ToString();
    end;
    Result := requestError;
  end;
end;

function gObtenerCarpetaDatos: string;
begin
{$IF DEFINED(IOS) or DEFINED(ANDROID)}
  Result := TPath.GetDocumentsPath;
{$ELSE}
  // Windows
  Result := TPath.Combine(TPath.GetHomePath(), 'Colector');
  TDirectory.CreateDirectory(Result);
{$ENDIF}
end;

function gGetPublicPath: string;
begin
{$IF DEFINED(IOS) or DEFINED(ANDROID)}
  Result := TPath.GetPublicPath;
{$ELSE}
  // Windows
  Result := TPath.Combine(TPath.GetPublicPath, 'Colector');
  TDirectory.CreateDirectory(Result);
{$ENDIF}
end;

function gObtenerRutaArchivo(nombre: string): string;
begin
  Result := TPath.Combine(gObtenerCarpetaDatos, nombre);
end;

function gObtenerRutaArchivoBD(usuario: string): string;
begin
  Result := TPath.Combine(gObtenerCarpetaDatos, usuario + '_db.s3db');
end;

function gObtenerRutaArchivoBDZip: string;
begin
  Result := TPath.Combine(gObtenerCarpetaDatos, 'bd.zip');
end;

function gObtenerRutaArchivoConfiguracion
  (usuario: string): string;
begin
  Result := TPath.Combine(gObtenerCarpetaDatos,
    usuario + '_config.bin');
end;

function gToBool(x: integer): Boolean;
begin
  if x > 0 then
    Result := True
  else
    Result := False;
end;

function gCargarCombo(cmb: TComboBox; elementos: TObjectList<AListableClass>): TComboBox;
var
  obj: AListableClass;
  Item: TListBoxItem;
begin
  cmb.ListBox.BeginUpdate;
  for obj in elementos do
  begin
    Item := TListBoxItem.Create(nil);
    Item.Text := obj.descripcion;
    Item.Parent := cmb.ListBox;
    Item.TextSettings.FontColor := obj.fontColor;
    Item.TextSettings.Font.Size := obj.fontSize;
    Item.StyledSettings := Item.StyledSettings - [TStyledSetting.FontColor, TStyledSetting.Size];
    Item.TagObject := obj;
  end;
  cmb.ListBox.EndUpdate;
  Result := cmb;
end;

end.