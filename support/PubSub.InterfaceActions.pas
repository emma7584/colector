unit PubSub.InterfaceActions;

interface

type
  TInterfaceAction = (actMostrarToast, actNuevoCodigo);
  TInterfaceActions = set of TInterfaceAction;

implementation

end.