unit PubSub.Provider;

interface

uses PubSub.Interfaces, System.Generics.Collections;

type
 TPubSubProvider = class (TInterfacedObject, IProviderInterface)
  private
    fSubscriberList: TList<ISubscriberInterface>;
  public
    procedure Subscribe(const tmpSubscriber: ISubscriberInterface);
    procedure Unsubscribe(const tmpSubscriber: ISubscriberInterface);
    procedure NotifySubscribers (const notifyClass: INotificationClass);

    constructor Create;
    destructor Destroy; override;
  end;

implementation

{ TPubSubProvider }

constructor TPubSubProvider.Create;
begin
  inherited;
  fSubscriberList:=TList<ISubscriberInterface>.Create;
end;

destructor TPubSubProvider.Destroy;
var
  iTemp: ISubscriberInterface;
begin
  for itemp in fSubscriberList do
      Unsubscribe(iTemp);
  fSubscriberList.Free;
  inherited;
end;

procedure TPubSubProvider.NotifySubscribers(const notifyClass: INotificationClass);
var
  tmpSubscriber: ISubscriberInterface;
begin
  for tmpSubscriber in fSubscriberList  do
    tmpSubscriber.UpdateSubscriber(notifyClass);
end;

procedure TPubSubProvider.Subscribe(const tmpSubscriber: ISubscriberInterface);
begin
  fSubscriberList.Add(tmpSubscriber);
end;

procedure TPubSubProvider.Unsubscribe(const tmpSubscriber: ISubscriberInterface);
begin
  fSubscriberList.Remove(tmpSubscriber);
end;

end.
