program Colector;

uses
  System.StartUpCopy,
  FMX.Forms,
  uPrincipal in 'views\uPrincipal.pas' {FmPrincipal},
  ViewModel.Principal in 'viewmodels\ViewModel.Principal.pas',
  Globales in 'support\Globales.pas',
  PubSub.InterfaceActions in 'support\PubSub.InterfaceActions.pas',
  PubSub.Interfaces in 'support\PubSub.Interfaces.pas',
  PubSub.Notifications in 'support\PubSub.Notifications.pas',
  PubSub.Provider in 'support\PubSub.Provider.pas',
  PubSub.Subscriber in 'support\PubSub.Subscriber.pas',
  Abstractas in 'support\Abstractas.pas',
  uScanner in 'views\uScanner.pas' {FmScanner},
  Records.Codigos in 'models\Records.Codigos.pas',
  FMX.Consts in 'FMX.Consts.pas',
  uRecepcionMercaderia in 'views\uRecepcionMercaderia.pas' {FmRecepcionMercaderia},
  Config in 'models\Config.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.Run;
end.
