alter PROCEDURE [dbo].[getRecepcionMercaderiaDetalle] (@idProveedor INT, @idTipoDoc VARCHAR(3), @idSucursal INT, @idNumDoc INT)
AS
BEGIN --EXEC getRecepcionMercaderiaDetalle 17648,'NP',2,1053
	SELECT CI.idNumLinea, CI.idProducto, P.sCodProducto, P.sCodBarra, P.sNombre, CI.iCantidad-CI.iCantidadRecibida AS iCantidad, CI.iCantidadRecibida
	FROM ComprasItems CI 
	 INNER JOIN Productos P on (CI.idProducto = P.idProducto)
	WHERE CI.idTipoDoc=@idTipoDoc
	 AND CI.idProveedor=@idProveedor
	 AND CI.idSucursal=@idSucursal
	 AND CI.idNumDoc=@idNumDoc
	 AND CI.iCantidad>CI.iCantidadRecibida
  END
GO 




