alter PROCEDURE [dbo].[SP_ProcesarComprasColectores]
AS
BEGIN
  BEGIN TRY
  BEGIN TRANSACTION;
  DECLARE  @Compras TABLE (idTipoDoc VARCHAR(3), idSucural INT, idProveedor INT, idNumDoc INT)
  DECLARE  @ComprasItems TABLE (idNumLinea INT, idProducto INT, rCantidadRecepcionada REAL, rCosto REAL, idLote INT, idDeposito INT)

  DECLARE @iTipoDoc VARCHAR(3) 
  DECLARE @iProveedor INT
  DECLARE @iSucursal INT
  DECLARE @iNumDoc INT
  DECLARE @idNumLinea INT 
  DECLARE @idProducto INT
  DECLARE @rCantidadRecepcionada REAL
  DECLARE @idSucCaja INT
  
  SET @idSucCaja=1;   
  IF OBJECT_ID('MSsubscription_properties') IS NOT NULL
   BEGIN
    SET @idSucCaja=IsNull((SELECT MIN(hostname) FROM MSsubscription_properties),1)
   END

  DECLARE @idTipoDoc VARCHAR(3)
  DECLARE @idNumDoc INT
  DECLARE @idLote INT
  DECLARE @idDeposito INT
  DECLARE @fDocumento DATETIME
  DECLARE @idUnidad INT
  DECLARE @rCosto REAL
  DECLARE @rSubBruto REAL
  DECLARE @rSubImpuesto REAL
  DECLARE @rSubNeto REAL
  DECLARE @sNumDoc VARCHAR(20)
  DECLARE @idLoteSuc INT
  DECLARE @idDepSuc INT

  SET  @idTipoDoc='CCA'
  SET  @fDocumento=GETDATE()

  DELETE @Compras
  INSERT INTO @Compras (idTipoDoc, idSucural, idProveedor, idNumDoc)
  SELECT DISTINCT idTipoDoc, idProveedor, idSucursal, idNumDoc 
  FROM TMPComprasColectora
  WHERE rCantidadRecepcionada>0

 DECLARE cab CURSOR FOR SELECT * FROM @Compras            
 OPEN cab 
  FETCH Next FROM cab INTO @iTipoDoc, @iProveedor, @iSucursal, @iNumDoc 
   WHILE @@fetch_status = 0
   BEGIN  
     SET  @idNumDoc=ISNULL((SELECT MAX(idNumDoc) FROM Compras WHERE idTipoDoc=@idTipoDoc AND idsucursal=@iSucursal AND idProveedor=@iProveedor),0)+1
	 SET @sNumDoc=(SELECT (@iTipoDoc+'-'+RIGHT('0000' + Cast(@iSucursal as Varchar),4)+'-'+RIGHT('00000000' + Cast(@iNumDoc as Varchar),8) ))
     --1.Inserto Compras
	 INSERT INTO Compras (idProveedor,idTipoDoc,idNumDoc,fDocumento,idMoneda,rTotBruto,rTotImpuestos,rTotDescuentos,rTotNeto,rCambio,fCarga,idOperador,fVencimiento,bAnulado,bFinalizado,
          idsucursal,idCondPagoCompra,bYaAfectoStock,rTotGravado,fContabilizar,bCredito,sObservaciones,cNCDisponible,idTipoCaja,idSucuCaja,bPendienteRecepcion,rImporteFacturaCompra)
     SELECT @iProveedor,@idTipoDoc,@idNumDoc,@fDocumento,2 AS idMoneda,0 AS rTotBruto,0 AS rTotImpuestos,0 AS rTotDescuentos,0 AS rTotNeto,1 AS rCambio,@fDocumento AS fCarga,'ArGestion' AS idOperador,@fDocumento AS fVencimiento,0 AS bAnulado,0 AS bFinalizado,
          @iSucursal,'EFE' AS idCondPagoCompra,1 AS bYaAfectoStock,0 AS rTotGravado,@fDocumento AS fContabilizar,0 AS bCredito,'Importacion Colector '+@sNumDoc AS sObservaciones,0 AS cNCDisponible,2 AS idTipoCaja,@idSucCaja,0 AS bPendienteRecepcion,0 AS rImporteFacturaCompra

	 --2.Inserto ComprasItems
     DELETE @ComprasItems;
     INSERT INTO @ComprasItems(idNumLinea,idProducto,rCantidadRecepcionada, rCosto, idLote, idDeposito)
     SELECT TMP.idNumLinea,TMP.idProducto,TMP.rCantidadRecepcionada, PC.rCosto, S.idLote, S.idDeposito
	 FROM TMPComprasColectora TMP, Stock S
	 LEFT JOIN PreciosDeCompra PC ON (PC.idLote=S.idLote AND PC.idDeposito=S.idDeposito)
	 WHERE TMP.idProducto=S.idProducto AND S.idDeposito=1
	  AND TMP.idTipoDoc=@iTipoDoc 
	  AND TMP.idProveedor=@iProveedor
	  AND TMP.idSucursal=@iSucursal
	  AND TMP.idNumDoc=@iNumDoc
	  AND TMP.rCantidadRecepcionada>0

		 DECLARE det CURSOR FOR SELECT * FROM @ComprasItems            
		 OPEN det 
		  FETCH Next FROM det INTO @idNumLinea,@idProducto,@rCantidadRecepcionada,@rCosto, @idLote, @idDeposito 
		   WHILE @@fetch_status = 0
		   BEGIN  
			  SET @rSubBruto=ROUND(@rCantidadRecepcionada*@rCosto,3)
			  SET @rSubImpuesto=ROUND(@rSubBruto*0.21,3)
			  SET @rSubNeto=ROUND(@rSubBruto+@rSubImpuesto,3)
			  SET @idDepSuc = ISNULL((SELECT MIN(DPV.idDeposito) FROM DepositoXPtoVta DPV, PuntosVenta PV
			                   WHERE DPV.idPV=PV.idPV AND PV.idSucursal=@idSucCaja),@idDeposito)
			  SET @idLoteSuc = (SELECT MIN(idLote) FROM Stock WHERE idProducto=@idProducto AND idDeposito=@idDepSuc)

		     INSERT INTO ComprasItems(idProveedor,idTipoDoc,idNumDoc,idNumLinea,idProducto,idUnidad,iCantidad,rPrecioUn,rSubBruto,rSubImpuestos,rSubDescuentos,rSubNeto,idDeposito,idLote,idsucursal,
		                              iCantidadRecibida,iCantUMxUnidad,IdRefNumLinea,rSubGravado,rSubExento,idRefTipoDoc,idRefProveedor,idRefSucursal,idRefNumDoc,idoperador,rPrecioOrig,iCantSub)
			 SELECT @iProveedor,@idTipoDoc,@idNumDoc,@idNumLinea,@idProducto,1 AS idUnidad,@rCantidadRecepcionada AS iCantidad,@rCosto AS rPrecioUn,@rSubBruto, @rSubImpuesto,0 AS rSubDescuentos,
			        @rSubNeto,@idDepSuc,@idLoteSuc,@iSucursal,0 AS iCantidadRecibida,1 AS iCantUMxUnidad,@idNumLinea AS IdRefNumLinea,@rSubBruto AS rSubGravado,0 rSubExento,@iTipoDoc AS idRefTipoDoc,@iProveedor AS idRefProveedor,
					@iSucursal AS idRefSucursal,@iNumDoc AS idRefNumDoc,'ArGestion' AS idoperador,@rCosto AS rPrecioOrig,0 AS iCantSub

			UPDATE ComprasItems
		    SET iCantidadRecibida=iCantidadRecibida+@rCantidadRecepcionada
		    WHERE idTipoDoc=@iTipoDoc 
		     AND idProveedor=@iProveedor
		     AND idSucursal=@iSucursal
		     AND idNumDoc=@iNumDoc
			 AND idNumLinea=@idNumLinea	

			FETCH next FROM det INTO @idNumLinea,@idProducto,@rCantidadRecepcionada,@rCosto, @idLote, @idDeposito  
		   END
		  CLOSE det
		  DEALLOCATE det
		  
		--3.Calculo total Compra
		UPDATE Compras
		SET rTotBruto=(SELECT SUM(CI.rSubBruto) FROM ComprasItems CI WHERE CI.idTipoDoc=Compras.idTipoDoc AND CI.idsucursal=Compras.idsucursal AND CI.idProveedor=Compras.idProveedor AND CI.idNumDoc=Compras.idNumDoc),
		    rTotNeto=(SELECT SUM(CI.rSubNeto) FROM ComprasItems CI WHERE CI.idTipoDoc=Compras.idTipoDoc AND CI.idsucursal=Compras.idsucursal AND CI.idProveedor=Compras.idProveedor AND CI.idNumDoc=Compras.idNumDoc),
		    rtotImpuestos=(SELECT SUM(CI.rSubImpuestos) FROM ComprasItems CI WHERE CI.idTipoDoc=Compras.idTipoDoc AND CI.idsucursal=Compras.idsucursal AND CI.idProveedor=Compras.idProveedor AND CI.idNumDoc=Compras.idNumDoc)
		WHERE idTipoDoc=@idTipoDoc 
		  AND idProveedor=@iProveedor
		  AND idSucursal=@iSucursal
		  AND idNumDoc=@idNumDoc
		
		--4.Marco Orden de compra como procesada
		UPDATE Compras
		SET bFinalizado=1
		WHERE idTipoDoc=@iTipoDoc 
		  AND idProveedor=@iProveedor
		  AND idSucursal=@iSucursal
		  AND idNumDoc=@iNumDoc	
		  AND NOT EXISTS (SELECT 1 FROM ComprasItems CI 
		                  WHERE CI.idTipoDoc=Compras.idTipoDoc AND CI.idProveedor=Compras.idProveedor 
						  AND CI.idsucursal=Compras.idsucursal AND CI.idNumDoc=Compras.idNumDoc
						  AND CI.iCantidad>CI.iCantidadRecibida)	

		--5.Elimino de la TMP  	
		DELETE TMPComprasColectora
		WHERE idTipoDoc=@iTipoDoc 
		  AND idProveedor=@iProveedor
		  AND idSucursal=@iSucursal
		  AND idNumDoc=@iNumDoc	

	FETCH next FROM cab INTO @iTipoDoc, @iProveedor, @iSucursal, @iNumDoc
   END
  CLOSE cab
  DEALLOCATE cab
  	
  COMMIT TRANSACTION;
 END TRY
   BEGIN CATCH
     ROLLBACK TRANSACTION;
       RAISERROR  ('Error al Procesar Compras ',16,1)
     RETURN(1)
    END CATCH
  RETURN(1)
END
  -- DELETE Compras WHERE fDocumento>='2020-03-11T00:00:00.000'
  -- SELECT * FROM Compras WHERE fDocumento>='2020-03-11T00:00:00.000'
  -- EXEC SP_ProcesarComprasColectores
  -- DELETE TMPComprasColectora where idnumdoc=288
  -- SELECT * FROM TMPComprasColectora order by idnumdoc, idnumlinea asc
  -- DELETE ComprasItems WHERE idTipoDoc='CCA' AND idNumDoc IN (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16)