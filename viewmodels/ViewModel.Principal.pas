unit ViewModel.Principal;

interface

uses
  PubSub.Interfaces;

type
  TPrincipalViewModel = class
    private
      fProvider: IProviderInterface;
    public
      property Provider: IProviderInterface read fProvider;
  end;

implementation

end.
