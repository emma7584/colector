unit Config;

interface

uses
  IniFiles;

type
  TConfiguracion = class(TMemIniFile)
  private
    function GetUltimaIP: string;
    procedure SetUltimaIP(const Value: string);
    function GetConexionAutomatica: integer;
    procedure SetConexionAutomatica(const Value: integer);
  public
    property UltimaIP: string read  GetUltimaIP write SetUltimaIP;
    property ConexionAutomatica: integer read  GetConexionAutomatica
                                          write SetConexionAutomatica;
  end;

implementation

function TConfiguracion.GetUltimaIP: string;
begin
  Result := Self.ReadString('Configuracion', 'ultimaip', '');
end;

procedure TConfiguracion.SetUltimaIP(const Value: string);
begin
  Self.WriteString('Configuracion', 'ultimaip', Value);
end;

function TConfiguracion.GetConexionAutomatica: integer;
begin
  Result := Self.ReadInteger('Configuracion', 'conexionautomatica', 0);
end;

procedure TConfiguracion.SetConexionAutomatica(const Value: integer);
begin
  Self.WriteInteger('Configuracion', 'conexionautomatica', Value);
end;

end.
